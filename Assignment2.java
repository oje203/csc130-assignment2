import java.util.Scanner;
public class Assignment2
{
	/*
	This program prompt five numbers and get the
	*sum
	*average
	*maximum number
	*minimum number
	 */
	public static void main(String[] args) 
	{
		/*
		Here we enable the program to add variables (numbers)
		 */
		Scanner keyboard = new Scanner(System.in);
		// And name those variables in this case we use n1,n2,etc
		int n1,n2,n3,n4,n5;
		// Now we write the method to add the numbers 
		System.out.println("Enter 5 numbers to get the sum");
		System.out.println("Enter the first number:"+" ");
		n1=keyboard.nextInt();
		System.out.println("Enter the second number:"+" ");
		n2=keyboard.nextInt();
		System.out.println("Enter the third number:"+" ");
		n3=keyboard.nextInt();
		System.out.println("Enter the fourth number:"+" ");
		n4=keyboard.nextInt();
		System.out.println("Enter the fifth number:"+" ");
		n5=keyboard.nextInt();
		//To prepare the sum
		int result=n1+n2+n3+n4+n5;
		//to display the sum
		System.out.println("The sum of these numbers is: "+result);
		//to prepare the average
		double Average = (n1+n2+n3+n4+n5)/5.0;
		//to display average
		System.out.println("The average is: "+ Average);
		//Create new String for each variable
		int [] numbers = new int [] {(int) n1,(int) n2,(int) n3,(int) n4,(int) n5};
		//create variable max
		int max = 0;
			//To display the maximum number
			 for (int i = 0; i < numbers.length; i ++)
			{
			             
			 if (numbers[i] > max)
				{
				max = numbers[i];              
			  	}
			            
			}
			System.out.println("The largest number is: " + max);
		//create variable min	          
		int min = (int) n1;
			//to display the minimum number
	            	for (int i = 0; i < numbers.length; i++)
			{
			if(numbers[i] < min)
				{
				min = numbers[i];
			    	}
			}
	         	System.out.println("The smallest number is: " + min);
	    
	}
				
		
}
